
/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
  console.log(err);
  return false;
})

context('Login', () => {

  const user = 'SYSTEM'
  const password = 'TZTUFN*'
  const enviroment = 'rec'
  const test_passwd = 'root'

  it('successfully login', () => {

    cy.visit('https://arilim.preprod-extranet.iga.fr/login')

    cy.get('input[name=uid]').type(user)
    cy.get('input[name=passwd]').type(password)
    cy.get('select#env').select(enviroment)
    cy.get('input[name=test_passwd]').type(test_passwd)

    cy.get('button[type=submit]').click({ force: true })

    cy.log('--- Force select ---')
    cy.get('.modal .bootbox-input-select').select('SYSTEM', { force: true })
    cy.get('.modal .bootbox-input-select').should('have.value', '11410303')

    // confirm Select2 widget renders the state name
    cy.get('.btn-primary').click({ force: true })

    cy.wait(3000)

    // Evaluer agrement
    cy.visit('https://arilim.preprod-extranet.iga.fr/agrements/evaluer-un-agrement')

    cy.log('--- Force select ---')
    cy.get('.modal .bootbox-input-select').select('A.B.C.R - NEOWI', { force: true })
    cy.get('.modal .bootbox-input-select').should('have.value', '11409874')

    cy.wait(3000)

    function generateRandomText() {
      let text = "";
      let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

      for (let i = 0; i < 10; i++)
        text += alphabet.charAt(Math.floor(Math.random() * alphabet.length))
      return text;
    }

    const generatedRandomText = generateRandomText()


    // cy.get('input[name=NOMBAIL]').type(generatedRandomText)
    // cy.get('input[name=PRENOMBAIL]').type(generatedRandomText)
    // cy.get('input[name=VOIEBAIL]').type(generatedRandomText)
    // cy.get('input[name=ADRESSEBAIL]').type(generatedRandomText)
    // cy.get('input[name=CPBAIL]').type(generatedRandomText)
    // cy.get('input[name=VILLEBAIL]').type(generatedRandomText)
    // cy.get('input[name=MTLOYER]').type(generatedRandomText)
    // cy.get('input[name=CHARGESMENS]').type(generatedRandomText)
    // cy.get('input[name=NBLOC]').type(generatedRandomText)
    // cy.get('input[name=LOC1NOM]').type(generatedRandomText)
    // cy.get('input[name=LOC1PRENOM]').type(generatedRandomText)
    // cy.get('input[name=LOC1STATUT]').type(generatedRandomText)
    // cy.get('input[name=LOC1REVENUAPL]').type(generatedRandomText)
    // cy.get('input[name=LOC1REVENUM1]').type(generatedRandomText)
    // cy.get('input[name=LOC1REVENUM2]').type(generatedRandomText)
    // cy.get('input[name=LOC1REVENUM3]').type(generatedRandomText)


  })
})

