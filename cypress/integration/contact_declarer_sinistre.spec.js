
/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
  console.log(err);
  return false;
})

context('Login', () => {

  const user = 'SYSTEM'
  const password = 'TZTUFN*'
  const enviroment = 'rec'
  const test_passwd = 'root'

  it('successfully login', () => {

    cy.visit('https://arilim.preprod-extranet.iga.fr/login')

    cy.get('input[name=uid]').type(user)
    cy.get('input[name=passwd]').type(password)
    cy.get('select#env').select(enviroment)
    cy.get('input[name=test_passwd]').type(test_passwd)

    cy.get('button[type=submit]').click({ force: true })

    cy.log('--- Force select ---')
    // by the rendered Select2 widget by using "force: true" option
    cy.get('.modal .bootbox-input-select').select('SYSTEM', { force: true })

    // confirm the value of the selected element
    cy.get('.modal .bootbox-input-select').should('have.value', '11410303')

    // confirm Select2 widget renders the state name
    cy.get('.btn-primary').click({ force: true })

    cy.wait(3000)

    // Contact
    cy.visit('https://arilim.preprod-extranet.iga.fr/accueil/contact')

    // - une police
    cy.visit('https://arilim.preprod-extranet.iga.fr/accueil/contact/formulaire-de-contact-lie-a-une-police?')


    cy.log('--- Force select ---')
    cy.get('.modal .bootbox-input-select').select('A PARTIR DE DESORMAIS', { force: true })
    cy.get('.modal .bootbox-input-select').should('have.value', '11387107')
    cy.get('.btn-primary').click({ force: true })

    cy.get('textarea#message').type('mensaje de test une police')
    // cy.get('.btn-primary').click({ force: true })

    // - une sinistre
    cy.visit('https://arilim.preprod-extranet.iga.fr/accueil/contact/formulaire-contact-lie-a-un-sinistre?')

    cy.log('--- Force select ---')
    cy.get('.modal .bootbox-input-select').select('ABBAYE D AINAY', { force: true })
    cy.get('.modal .bootbox-input-select').should('have.value', '11382960')
    cy.get('.btn-primary').click({ force: true })

    cy.wait(3000)
    cy.log('--- Force select ---')
    cy.get('.modal .bootbox-input-select').select('MR1826929SF 01/06/2018', { force: true })
    cy.get('.modal .bootbox-input-select').should('have.value', '11061705')
    cy.get('.btn-primary').click({ force: true })

    cy.get('textarea#message').type('mensaje de test une sinistre')
    // cy.get('.btn-primary').click({ force: true })

    // - une quittance
    cy.visit('https://arilim.preprod-extranet.iga.fr/accueil/contact/formulaire-contact-lie-a-une-quittance?')

    cy.log('--- Force select ---')
    cy.get('.modal .bootbox-input-select').select('AAPL MR LESTRINGANT PASCAL', { force: true })
    cy.get('.modal .bootbox-input-select').should('have.value', '11406637')
    cy.get('.btn-primary').click({ force: true })

    cy.wait(3000)

    cy.log('--- Force select ---')
    cy.get('.modal .bootbox-input-select').select('73280 - 05/12/2018 au 31/05/2019', { force: true })
    cy.get('.modal .bootbox-input-select').should('have.value', '11186676')
    cy.get('.btn-primary').click({ force: true })
    cy.get('textarea#message').type('mensaje de test une quittance')

    // - autre
    cy.visit('https://arilim.preprod-extranet.iga.fr/accueil/contact/formulaire-de-contact-simple?')
    cy.get('input[name=objet]').type(user)
    cy.get('textarea#message').type('mensaje de test une contact-simple')
    // cy.get('.btn-primary').click({ force: true })


  })
})

